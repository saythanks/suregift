<?php

return [
    'api' => [
        'host' => env('SUREGIFT_API_HOST', 'http://staging.business.suregifts.com.ng/api/business/'),
        'username' => env('SUREGIFT_API_USERNAME', ''),
        'password' => env('SUREGIFT_API_PASSWORD', ''),
    ]

];
