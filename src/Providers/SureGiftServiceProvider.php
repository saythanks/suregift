<?php

namespace SayThanks\SureGift\Providers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;
use SayThanks\SureGift\SureGiftClient;

class SureGiftServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/suregift.php',
            'suregift'
        );

        $this->app->bind(
            'suregift',
            static function ($app, $params) {
                config('suregift.api.username');
                config('suregift.api.password');
                $username = $params['username'] ?? config('suregift.api.username');
                $password = $params['password'] ?? config('suregift.api.password');

                if ($username === null) {
                    throw new BindingResolutionException(
                        'Cannot create SureGift Client: Username is missing. 
                        Please ensure SUREGIFT_API_USERNAME is in your .env or provide a username when creating the client.'
                    );
                }

                if ($password === null) {
                    throw new BindingResolutionException(
                        'Cannot create SureGift Client: Password is missing. 
                        Please ensure SUREGIFT_API_PASSWORD is in your .env or provide a password when creating the client.'
                    );
                }

                return new SureGiftClient($username, $password);
            }
        );
    }
}
