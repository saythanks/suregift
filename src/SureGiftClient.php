<?php

namespace SayThanks\SureGift;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

final class SureGiftClient
{
    public PendingRequest $client;

    public function __construct(string $username, string $password)
    {
        $this->client = Http::retry(3, 100)
            ->baseUrl(config('suregift.api.host'))
            ->asJson()
            ->acceptJson()
            ->withHeaders([
                'Authorization' => 'Basic ' . base64_encode($username . ':' . $password)
            ]);
    }

    public function getMerchants(): array
    {
        $response = $this->get('GetJSONMerchants');

        return $response->json();
    }

    public function findMerchant(string $merchantId): array
    {
        $response = $this->get('GetJSONMerchants', ['merchantId' => $merchantId]);

        return $response->json();
    }

    public function getSavedTemplate(): array
    {
        $response = $this->get('JSONGetSavedTemplate');

        return $response->json();
    }

    public function getWalletBalance(): array
    {
        $response = $this->get('JSONCheckWalletBalance');

        return $response->json();
    }

    public function getOrderConfirmation(string $orderId): array
    {
        $response = $this->get('JSONOrderConfirmation', ['orderId' => $orderId]);

        return $response->json();
    }

    public function processBulkOrder(array $data): array
    {
        $missingRequestData = $this->findMissingBulkOrderData($data);
        if (!empty($missingRequestData)) {
            Log::error('SureGiftClient: missing processBulkOrder data', $missingRequestData);
            throw new \InvalidArgumentException('Missing processBulkOrder data. See logs for more information.');
        }
        $response = $this->post('JSONProcessBulkOrder', $data);
        return $response->json();
    }

    private function get(string $url, array $queryData = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        return $this->request('get', $url, $queryData);
    }

    private function post(string $url, array $data = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        return $this->request('post', $url, $data);
    }

    private function request(string $method, string $url, array $data = []): \GuzzleHttp\Promise\PromiseInterface|Response
    {
        if ($method !== 'get' && $method !== 'post') {
            throw new \InvalidArgumentException("SureGiftClient: unsupported request type $method specified - expected 'get' or 'post'.");
        }

        Log::info("SureGiftClient: Sending $method request", ['url' => $url, 'query' => $data]);
        /** @var \GuzzleHttp\Promise\PromiseInterface|Response $response */
        $response = $this->client->$method($url, $data);

        $logData = [
            'url' => $url,
            'data' => $data,
            'response_code' => $response->status(),
            'response_json' => $response->json(),
        ];
        if ($response->successful()) {
            Log::debug("SureGiftClient: $method request successful", $logData);
        } else {
            Log::warning("SureGiftClient: $method request unsuccessful", $logData);
        }
        return $response;
    }

    private function findMissingBulkOrderData(array $data): array
    {
        $missingData = [];
        $expectedKeys = ['DeliveryDate', 'OrderItemRequests'];
        //TemplateId is optional
        $expectedOrderItemKeys = [
            'MerchantId',
            'SpecialMessage',
            'EventName',
            'TemplateType',
            'Amount',
            'Quantity',
            'RecipientBatchName',
            'RecipientEmail',
            'RecipientPhone',
            'RecipientFirstName',
            'RecipientLastName'
        ];

        $missingTopLevelKeys = array_diff($expectedKeys, array_keys($data));
        if (!empty($missingTopLevelKeys)) {
            if (in_array('OrderItemRequests', $missingTopLevelKeys)) {
                return $missingTopLevelKeys;
            }
            $missingData['topLevelKeys'] = $missingTopLevelKeys;
        }
        $i = 0;
        foreach($data['OrderItemRequests'] as $orderItem) {
            if (!is_array($orderItem)) {
                $missingData['orderItems'][$i] = 'Order item is not array.';
            } else {
                $missingOrderItemKeys = array_diff($expectedOrderItemKeys, array_keys($orderItem));
                if (!empty($missingOrderItemKeys)) {
                    $missingData['orderItems'][$i] = $missingOrderItemKeys;
                }
            }
            $i++;
        }

        return $missingData;
    }
}
