<?php

namespace SayThanks\SureGift\Facades;

use Illuminate\Support\Facades\Facade;

class SureGiftFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'suregift';
    }
}
